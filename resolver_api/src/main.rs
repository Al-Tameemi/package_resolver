use axum::{
    debug_handler,
    extract::State,
    http::StatusCode,
    response::{IntoResponse, Response},
    routing::{get, post},
    Json, Router,
};
use clap::Parser;
use log::{debug, error, info};
use npm_resolver::resolve;
use resolver_types::PackageRequest;
use std::net::SocketAddr;
use std::str::FromStr;
use tower_http::cors::CorsLayer;

#[derive(Clone)]
struct AppState {}

/// A web API to resolve NPM dependencies
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Address to bind the API to
    #[arg(short, long, default_value = "0.0.0.0:8080")]
    binding_address: String,
}

fn error_and_panic(error_message: String) {
    error!("{}", error_message);
    panic!("{}", error_message);
}

#[tokio::main]
async fn main() {
    match env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("warn"))
        .try_init()
    {
        Ok(_) => debug!("Logger initialized!"),
        Err(e) => error!("Failed to initialize logger: {e}"),
    };

    let args = Args::parse();
    let crate_resolver: crate_resolver::Resolver = crate_resolver::Resolver::new().unwrap();

    let app = Router::new()
        .route("/", get(root))
        .route("/resolve_npm", post(resolve_npm))
        .route("/resolve_crate", post(resolve_crate))
        .with_state(crate_resolver)
        .layer(CorsLayer::permissive());

    let address = SocketAddr::from_str(&args.binding_address).unwrap_or_else(|e| {
        panic!("Failed to parse address {}: {e}", &args.binding_address);
    });

    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap_or_else(|e| {
            error_and_panic(format!("Failed to start serving: {e}"));
        });
}

/// Simply greets anyone coming to the root
async fn root() -> &'static str {
    "Hello World!"
}

/// Resolves the requested package and its dependencies.
/// Returns a list of packages, their versions, and their download link
async fn resolve_npm(Json(payload): Json<PackageRequest>) -> Response {
    debug!("Received request: {:#?}", payload);

    let resolved_packages = resolve(payload.packages).await;
    if resolved_packages.len() <= 1 {
        let key = resolved_packages.keys().collect::<Vec<&String>>()[0];
        if resolved_packages.get(key).unwrap().is_none() {
            return StatusCode::NOT_FOUND.into_response();
        }
    }
    info!("Resolved: `{}` packages", resolved_packages.keys().len());
    Json(resolved_packages).into_response()
}

/// Resolves the requested package and its dependencies.
/// Returns a list of packages, their versions, and their download link
async fn resolve_crate(
    State(resolver): State<crate_resolver::Resolver>,
    Json(payload): Json<PackageRequest>,
) -> Response {
    debug!("Received request: {:#?}", payload);
    let resolved_packages = resolver.resolve(payload.packages);
    if resolved_packages.len() <= 1 {
        let key = resolved_packages.keys().collect::<Vec<&String>>()[0];
        if resolved_packages.get(key).unwrap().is_none() {
            return StatusCode::NOT_FOUND.into_response();
        }
    }
    info!("Resolved: `{}` packages", resolved_packages.keys().len());
    Json(resolved_packages).into_response()
}
