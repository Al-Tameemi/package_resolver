import { useState, ChangeEvent, FormEvent } from "react";
import "./App.css";
import axios from "axios";

const apiURI = "http://localhost:8080";


interface InputField {
  name: string;
  version: string;
}

interface PackageInfo {
  name: string;
  resolved_version: string;
  download_link: string;
  dependency_type: string;
}

function App() {
  const [dependencyType, setDependencyType] = useState("npm");
  const [inputFields, setInputFields] = useState<InputField[]>([
    { name: "", version: "" },
  ]);
  const [dependencies, setDependencies] = useState(null);

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log("submitted the form", { packages: inputFields });
    try {
      if (dependencyType === "npm") {
        console.log("making request to npm package resolver");
        const result = await axios.post(`${apiURI}/resolve_npm`, {
          packages: inputFields,
        });
        console.log("result", result.data);
        setDependencies(result.data);
        console.log("dependencies: ", dependencies);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const addInputField = () => {
    setInputFields([...inputFields, { name: "", version: "" }]);
  };

  const deleteInputField = (index:number) => {
    const updatedInputFields = [...inputFields]
    updatedInputFields.splice(index,1)
    setInputFields(updatedInputFields)
  }

  const handleOptionChange = (e: ChangeEvent<HTMLInputElement>) => {
    setDependencyType(e.target.value);
  };

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const values = [...inputFields];
    if (e.target.name === "name") {
      values[index].name = e.target.value;
    } else {
      values[index].version = e.target.value;
    }
    setInputFields(values);
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          type='radio'
          id='npm-radio'
          name='dependency'
          value='npm'
          checked={dependencyType === "npm"}
          onChange={handleOptionChange}
        />
        <label htmlFor='npm-radio'>npm</label>
        <input
          type='radio'
          id='cargo-radio'
          name='dependency'
          value='cargo'
          checked={dependencyType === "cargo"}
          onChange={handleOptionChange}
          disabled
        />
        <label htmlFor='cargo-radio'>cargo</label>
        <span>(coming soon!)</span>
        <br />
        <button type='button' onClick={addInputField}>
          Add row
        </button>
        <button type='submit'>Send</button>
        {inputFields.map((field, index) => {
          return (
            <div key={index}>
              <label htmlFor='name'>Name:</label>
              <input
                type='text'
                id='name'
                name='name'
                value={field.name}
                onChange={(e) => handleInputChange(e, index)}
              />
              <label htmlFor='version'>Version:</label>
              <input
                type='text'
                id='version'
                name='version'
                value={field.version}
                onChange={(e) => handleInputChange(e, index)}
              />
              <button type="button" onClick={()=>deleteInputField(index)}>Remove</button>
            </div>
          );
        })}
      </form>
      {dependencies && (
        <>
          <table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Version</th>
                <th>Download Link</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(dependencies).map((value, index) => {
                return (
                  <tr key={index}>
                    <td>{value.name}</td>
                    <td>{value.resolved_version}</td>
                    <td>
                      <a href={value.download_link}>
                        {value.download_link}
                      </a>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      )}
    </>
  );
}

export default App;
