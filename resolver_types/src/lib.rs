use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Represents the request format received from the user
#[derive(Debug, Serialize, Deserialize)]
pub struct PackageRequest {
    pub packages: Vec<RequestedPackage>,
}

/// The package requested, where the version string would be resolved into a valid version
#[derive(Debug, Serialize, Deserialize)]
pub struct RequestedPackage {
    pub name: String,
    pub version: String,
    #[serde(default)]
    pub dependency_type: DependencyType,
    // depth: Option<u32>,
}

/// Struct expected to be used by the resolver libraries to represent a resolved package
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ResolvedPackage {
    pub name: String,
    pub resolved_version: String,
    pub download_link: String,
    pub dependency_type: DependencyType,
}

pub trait Resolver {
    fn resolve(requested_packages_list: Vec<RequestedPackage>) -> HashMap<String, ResolvedPackage>;
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, Default)]
pub enum DependencyType {
    DevDependency,
    #[default]
    Dependency,
}
