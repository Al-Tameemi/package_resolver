#[derive(thiserror::Error, Debug)]
pub enum CrateError {
    #[error("Failed find crate: {name}")]
    CrateNotFound { name: String },
    #[error("Failed to resolve crate `{name}` with version `{version}`")]
    CrateResolveError { name: String, version: String },
    #[error(transparent)]
    CratesIndexError(#[from] crates_index::error::Error),
    #[error(transparent)]
    CargoSemverError(#[from] semver::Error),
    #[error("Failed to lock index")]
    FailedToLockIndex,
}
