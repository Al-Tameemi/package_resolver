use crates_index::{Index, IndexConfig};
use dashmap::DashMap;
use log::error;
use resolver_types::{RequestedPackage, ResolvedPackage};
use semver::VersionReq;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

mod error;
use error::CrateError;

#[derive(Clone)]
pub struct Resolver {
    index: Arc<Mutex<Index>>,
}

impl Resolver {
    pub fn new() -> Result<Resolver, crates_index::error::Error> {
        let index = crates_index::Index::new_cargo_default()?;
        Ok(Resolver {
            index: Arc::new(Mutex::new(index)),
        })
    }

    pub fn resolve(
        &self,
        requested_packages_list: Vec<RequestedPackage>,
    ) -> HashMap<String, Option<ResolvedPackage>> {
        let resolved_packages = Arc::new(DashMap::new());
        Resolver::recursive_resolve(
            self.index.clone(),
            requested_packages_list,
            resolved_packages.clone(),
        );
        resolved_packages
            .as_ref()
            .iter()
            .map(|ref_multi| (ref_multi.key().clone(), ref_multi.value().clone()))
            .collect::<HashMap<String, Option<ResolvedPackage>>>()
    }

    fn recursive_resolve(
        index: Arc<Mutex<Index>>,
        requested_packages_list: Vec<RequestedPackage>,
        resolved_packages: Arc<DashMap<String, Option<ResolvedPackage>>>,
    ) {
        let mut handles = Vec::new();
        for requested_package in requested_packages_list {
            let resolved_packages = resolved_packages.clone();
            let index = index.clone();
            handles.push(std::thread::spawn(move || {
                if resolved_packages.get(&requested_package.name).is_none() {
                    if let Ok(package) = Resolver::resolve_package(
                        index.clone(),
                        requested_package.name.clone(),
                        requested_package.version.clone(),
                    ) {
                        let index_config = match index.lock() {
                            Ok(index) => match index.index_config() {
                                Ok(config) => config,
                                Err(e) => {
                                    let dl = "https://crates.io/api/v1/crates".to_owned();
                                    error!("Failed to resolve index api, using `{dl}` instead: {e}");
                                    IndexConfig {
                                        dl,
                                        api: Some("https://crates.io".to_owned()),
                                    }
                                }
                            },
                            Err(e) => {
                                resolved_packages.insert(
                                    format!(
                                        "{name}@{version}",
                                        name = requested_package.name,
                                        version = requested_package.version
                                    ),
                                    None,
                                );
                                error!("Failed to lock index: {e}");
                                return;
                            }
                        };
                        let download_link = if let Some(link) = package.download_url(&index_config)
                        {
                            link
                        } else {
                            let name = package.name();
                            let version = package.version();
                            format!("https://crates.io/api/v1/crates/{name}/{version}/download")
                        };
                        let resolved_data = ResolvedPackage {
                            name: package.name().to_string(),
                            resolved_version: package.version().to_string(),
                            download_link,
                            dependency_type: Default::default(),
                        };

                        resolved_packages.insert(
                            format!(
                                "{name}@{version}",
                                name = requested_package.name,
                                version = requested_package.version
                            ),
                            Some(resolved_data),
                        );

                        let dependencies: Vec<RequestedPackage> = package
                            .dependencies()
                            .iter()
                            .map(|dependency| RequestedPackage {
                                name: dependency.name().to_string(),
                                version: dependency.requirement().to_string(),
                                dependency_type: Default::default(),
                            })
                            .collect::<Vec<RequestedPackage>>();

                        Resolver::recursive_resolve(index.clone(), dependencies, resolved_packages);
                    } else {
                        resolved_packages.insert(
                            format!(
                                "{name}@{version}",
                                name = requested_package.name,
                                version = requested_package.version
                            ),
                            None,
                        );
                    }
                }
            }));
        }
        for handle in handles {
            let _ = handle.join();
        }
    }

    fn resolve_package(
        index: Arc<Mutex<Index>>,
        name: String,
        version: String,
    ) -> Result<crates_index::Version, CrateError> {
        if let Ok(index) = index.lock() {
            if let Some(requested_crate) = index.crate_(&name) {
                let requested_version = VersionReq::parse(&version)?;
                let versions = requested_crate.versions();
                for ii in (0..versions.len()).rev() {
                    let crate_release = &versions[ii];
                    let crate_version = semver::Version::parse(crate_release.version())?;
                    if requested_version.matches(&crate_version) && !crate_release.is_yanked() {
                        return Ok(crate_release.clone());
                    }
                }
                Err(CrateError::CrateResolveError { name, version })
            } else {
                Err(CrateError::CrateNotFound { name })
            }
        } else {
            Err(CrateError::FailedToLockIndex)
        }
    }
}
