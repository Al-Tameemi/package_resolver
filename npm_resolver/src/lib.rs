use crate::error::NpmResolverError::UnresolvedPackage;
use crate::npm_api::{ResolverResult, VersionedPackage};
use async_recursion::async_recursion;
use dashmap::DashMap;
use log::{debug, error};
use node_semver::{Range, Version};
use resolver_types::{RequestedPackage, ResolvedPackage};
use std::collections::HashMap;
use std::sync::Arc;

mod error;
mod npm_api;

pub async fn resolve(
    requested_packages_list: Vec<RequestedPackage>,
) -> HashMap<String, Option<ResolvedPackage>> {
    println!("Resolving...");
    let resolved_packages = Arc::new(DashMap::new());
    recursive_resolve(requested_packages_list, resolved_packages.clone()).await;
    resolved_packages
        .as_ref()
        .iter()
        .map(|ref_multi| (ref_multi.key().clone(), ref_multi.value().clone()))
        .collect::<HashMap<String, Option<ResolvedPackage>>>()
}

#[async_recursion]
pub async fn recursive_resolve(
    requested_packages_list: Vec<RequestedPackage>,
    resolved_packages: Arc<DashMap<String, Option<ResolvedPackage>>>,
) {
    for package in requested_packages_list {
        // Package already resolved/being resolve, no need to do that again
        let resolved_packages = resolved_packages.clone();
        let _ = tokio::spawn(async move {
            if resolved_packages.get(&package.name.clone()).is_none() {
                match resolve_package(package.name.clone(), package.version.clone()).await {
                    Ok(data) => {
                        let resolved_data = ResolvedPackage {
                            name: data.name().to_string(),
                            resolved_version: data.version().to_string(),
                            download_link: data.dist().tarball().to_string(),
                            dependency_type: package.dependency_type,
                        };
                        resolved_packages.insert(
                            format!(
                                "{name}@{version}",
                                name = package.name,
                                version = package.version
                            ),
                            Some(resolved_data),
                        );

                        let dependencies: Vec<RequestedPackage> = data
                            .dependencies()
                            .iter()
                            .map(|(package, version)| RequestedPackage {
                                name: package.clone(),
                                version: version.clone(),
                                dependency_type: Default::default(),
                            })
                            .collect::<Vec<RequestedPackage>>();
                        //  TODO: Maybe later?
                        // let dev_dependencies: Vec<RequestedPackage> = data
                        //     .dev_dependencies()
                        //     .iter()
                        //     .map(|(package, version)| {
                        //         RequestedPackage {
                        //             name: package.clone(),
                        //             version: version.clone(),
                        //             dependency_type: DependencyType::DevDependency,
                        //         }
                        //     })
                        //     .collect::<Vec<RequestedPackage>>();
                        recursive_resolve(dependencies, resolved_packages.clone()).await;
                        // TODO: maybe do later
                        // recursive_resolve(&dev_dependencies, resolved_packages.clone()).await;
                    }
                    Err(e) => {
                        error!("Failed to resolve package: {e}");
                        resolved_packages.insert(
                            format!(
                                "{name}@{version}",
                                name = package.name,
                                version = package.version
                            ),
                            None,
                        );
                    }
                }
            }
        })
        .await;
    }
}

/// Resolves the desired version for one package
pub async fn resolve_package(name: String, version: String) -> ResolverResult<VersionedPackage> {
    debug!("Parsing version {} for {}", &version, &name);
    let desired_version: Range = version.parse()?;
    let unversioned_package_data = npm_api::fetch_unversioned(name.clone()).await?;
    let mut sorted_versions = unversioned_package_data
        .versions()
        .keys()
        .filter_map(|version_string| match version_string.parse::<Version>() {
            Ok(version) => Some(version),
            Err(_) => None,
        })
        .collect::<Vec<Version>>();
    sorted_versions.sort();
    sorted_versions.reverse();
    debug!("looking to satisfy {version} for {name}");

    for available_version in sorted_versions {
        if available_version.satisfies(&desired_version) {
            debug!("{} satisfies {}", &available_version, &desired_version);
            let version_data = unversioned_package_data
                .versions()
                .get(&available_version.to_string());
            match version_data {
                Some(data) => return Ok(data.clone()),
                None => return Err(error::NpmResolverError::MissingPackageData { name, version }),
            }
        }
    }

    Err(UnresolvedPackage { name, version })
}

#[cfg(test)]
mod tests {
    use crate::{resolve, resolve_package};
    use resolver_types::RequestedPackage;

    #[test]
    fn sanity() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }

    #[tokio::test]
    async fn resolve_package_test() {
        env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug"))
            .try_init()
            .unwrap();

        let package = String::from("eslint");
        let version = String::from("^4.0.0");

        let returned_package = resolve_package(package, version).await;
        if let Ok(returned_package) = returned_package {
            assert_eq!(returned_package.version(), "4.19.1");
        } else {
            panic!("No package was returned");
        }
    }

    #[tokio::test]
    async fn resolve_exact_version_test() {
        env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug"))
            .try_init()
            .unwrap();
        let package = String::from("linefix");
        let version = String::from("0.1.1");

        let returned_package = resolve_package(package, version).await;
        if let Ok(returned_package) = returned_package {
            assert_eq!(returned_package.version(), "0.1.1");
        } else {
            panic!("No package was returned");
        }
    }

    #[tokio::test]
    async fn resolve_graph() {
        env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug"))
            .try_init()
            .unwrap();

        let package = String::from("eslint");
        let version = String::from("8.41.0");
        let package_request = RequestedPackage {
            name: package,
            version,
            dependency_type: Default::default(),
        };
        let resolved_packages = resolve(vec![package_request]).await;
        println!("{resolved_packages:#?}");
    }
}
