use log::debug;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

pub type ResolverResult<T> = Result<T, crate::error::NpmResolverError>;

/// Fetches the data for the all package versions from NPM's API
pub async fn fetch_unversioned(package: String) -> ResolverResult<UnversionedPackage> {
    debug!("Fetching data for `{}`", &package);
    let url = generate_url(package, None);
    let request = &reqwest::get(url).await?.text().await?;
    let data: UnversionedPackage = serde_json::from_str(request)?;
    Ok(data)
}

#[allow(dead_code)]
// Probably will be used later. No harm in keeping it
/// Fetches the data for the specific version of the package from NPM's API
pub async fn fetch_versioned(package: String, version: String) -> ResolverResult<VersionedPackage> {
    debug!("Fetching data for `{}@{}`", &package, &version);
    let url = generate_url(package, Some(version));
    let data: VersionedPackage = serde_json::from_str(&reqwest::get(url).await?.text().await?)?;
    Ok(data)
}

/// Generates the registry URL to fetch the package data from
fn generate_url(package: String, version: Option<String>) -> String {
    let version = match version {
        Some(version) => version,
        None => String::from(""),
    };
    format!("https://registry.npmjs.org/{package}/{version}")
}

/// Represents the data for a specific version of a package
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct VersionedPackage {
    name: String,
    version: String,
    dist: Dist,
    #[serde(rename = "_npmVersion")]
    npm_version: Option<String>,
    #[serde(default)]
    dependencies: HashMap<String, String>,
    #[serde(rename = "devDependencies", default)]
    dev_dependencies: HashMap<String, String>,
    license: Option<Value>,
}

impl VersionedPackage {
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn version(&self) -> &str {
        &self.version
    }
    pub fn dist(&self) -> &Dist {
        &self.dist
    }
    pub fn npm_version(&self) -> &Option<String> {
        &self.npm_version
    }
    pub fn dependencies(&self) -> &HashMap<String, String> {
        &self.dependencies
    }
    pub fn dev_dependencies(&self) -> &HashMap<String, String> {
        &self.dev_dependencies
    }
    pub fn license(&self) -> &Option<Value> {
        &self.license
    }
}

/// Represents the general data for a package
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UnversionedPackage {
    name: String,
    #[serde(rename = "dist-tags")]
    dist_tags: Option<DistTags>,
    versions: HashMap<String, VersionedPackage>,
}

impl UnversionedPackage {
    #[allow(dead_code)]
    // Probably will use later, no harm in keeping it.
    pub fn name(&self) -> &str {
        &self.name
    }
    #[allow(dead_code)]
    pub fn dist_tag(&self) -> &Option<DistTags> {
        &self.dist_tags
    }
    pub fn versions(&self) -> &HashMap<String, VersionedPackage> {
        &self.versions
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DistTags {
    latest: Option<String>,
    next: Option<String>,
}

impl DistTags {
    #[allow(dead_code)]
    pub fn latest(&self) -> &Option<String> {
        &self.latest
    }
    #[allow(dead_code)]
    pub fn next(&self) -> &Option<String> {
        &self.next
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Dist {
    shasum: String,
    tarball: String,
    integrity: Option<String>,
}

impl Dist {
    pub fn shasum(&self) -> &str {
        &self.shasum
    }
    pub fn tarball(&self) -> &str {
        &self.tarball
    }
    pub fn integrity(&self) -> &Option<String> {
        &self.integrity
    }
}

#[cfg(test)]
mod test {
    use crate::npm_api::fetch_unversioned;

    #[tokio::test]
    async fn fetch_test() {
        env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug"))
            .try_init()
            .unwrap();
        let package = String::from("eslint");
        let data = fetch_unversioned(package).await;
        assert!(data.is_ok());
        let data = data.unwrap();
        assert_eq!(data.name(), "eslint");
    }
}
