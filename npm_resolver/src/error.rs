#[derive(thiserror::Error, Debug)]
pub enum NpmResolverError {
    #[error("Failed to fetch data.")]
    CouldNotFetchData(#[from] reqwest::Error),
    // #[error("Failed to deserialize data for `{package:}`: {error:}")]
    // InvalidResponseData {
    //     package: String,
    //     error: serde_json::Error,
    // }
    #[error(transparent)]
    UnexpectedResponseData(#[from] serde_json::Error),
    #[error("Failed to resolve package: {name} with version: {version}")]
    UnresolvedPackage {
        name: String,
        version: String,
    },
    #[error("Package data missing for {name}@{version}")]
    MissingPackageData {
        name: String,
        version: String,
    },
    #[error("Failed to parse verison.")]
    VersionParseFailed(#[from] node_semver::SemverError)
}
